import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { CurrencyValues } from 'src/app/models/currency-values';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangeDetectionStrategy } from '@angular/core';
import { Subscription, interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CurrenciesComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<boolean>();
  currencyValuesList: CurrencyValues[];
  subscription: Subscription;
  usdToPlnValue: number;

  constructor(private cd: ChangeDetectorRef, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.currencyValuesList = this.route.snapshot.data.currencyValuesList;
    var reloadNumber = 0;
    const source = interval(5000);
    source.pipe(takeUntil(this.destroy$)).subscribe(() => {
      reloadNumber++;
      this.router.navigate(['/features/currencies/' + reloadNumber]);
      this.cd.detectChanges();
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
