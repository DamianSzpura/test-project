import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[bidValueRef]'
})
export class BidValueRefDirective {
  private bidValueNativeElement: any;

  constructor(element: ElementRef) {
    this.bidValueNativeElement = element.nativeElement;
  }

  public get getBidValueNativeElement() {
    return this.bidValueNativeElement;
  }
}
