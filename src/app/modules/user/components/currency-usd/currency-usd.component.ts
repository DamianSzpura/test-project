import { Component, ContentChild, AfterViewInit, Renderer2 } from '@angular/core';
import { BidValueRefDirective } from './bid-value-ref.directive';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'currency-usd',
  templateUrl: './currency-usd.component.html',
  styleUrls: ['./currency-usd.component.less']
})
export class CurrencyUsdComponent implements AfterViewInit {
  plnToUsdValue: number = 3;
  constructor(private renderer: Renderer2) {}

  @ContentChild(BidValueRefDirective)
  bidValue: BidValueRefDirective;

  public USDbid(bid: number): number {
    return bid / this.plnToUsdValue;
  }

  ngAfterViewInit() {
    var usdBidvalue = formatNumber(
      this.bidValue.getBidValueNativeElement.innerText / this.plnToUsdValue,
      'en-En',
      '1.1-1'
    );
    this.renderer.setProperty(this.bidValue.getBidValueNativeElement, 'innerHTML', usdBidvalue);
  }
}
