import { Injectable } from '@angular/core';
import { BitbayApiService } from './bitbay-api.service';

import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { CurrencyValues } from 'src/app/models/currency-values';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  private usdToPlnValue: number;
  currencyValuesList: {
    currency: string;
  }[];

  constructor(private bitbayApi: BitbayApiService) {
    this.currencyValuesList = [
      {
        currency: 'BTC'
      },
      {
        currency: 'LSK'
      },
      {
        currency: 'ETH'
      }
    ];

    this.usdToPlnValue = 3;
  }

  public get getCurrencyValuesList() {
    return forkJoin(
      this.getCurrencyValue(this.currencyValuesList[0]),
      this.getCurrencyValue(this.currencyValuesList[1]),
      this.getCurrencyValue(this.currencyValuesList[2])
    ).pipe(
      map(data => {
        return data;
      })
    );
  }

  private getCurrencyValue(currencyValues: CurrencyValues) {
    return this.bitbayApi.get(currencyValues.currency, 'PLN').pipe(
      map(
        data => {
          return {
            currency: currencyValues.currency,
            bidValue: data.bid,
            bidValueUSD: data.bid / this.usdToPlnValue
          };
        },
        error => {
          console.log("Couldn't get data from " + currencyValues.currency + ' and PLN');
        }
      )
    );
  }
}
