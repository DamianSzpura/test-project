import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthGuard } from './guards/auth.guard';

const Routes: Routes = [
  {
    path: 'features',
    loadChildren: 'src/app/modules/user/user.module#UserModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: 'src/app/modules/security/security.module#SecurityModule'
  },
  {
    path: '**',
    redirectTo: '/404',
    pathMatch: 'full'
  },
  {
    path: '404',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(Routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
